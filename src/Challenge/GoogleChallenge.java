package Challenge;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.StaleElementReferenceException;

public class GoogleChallenge {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement textfield = driver.findElement(By.name("q"));
		Actions action = new Actions(driver);
		
		action.moveToElement(textfield).sendKeys("irbid").build().perform();
		
		WebElement ll = driver.findElement(By.xpath("//ul[@class='erkvQe']"));
		List <WebElement> allsug = ll.findElements(By.tagName("span"));
		
		for(int i=0; i<(allsug.size()); i++)
		{
			System.out.println(allsug.get(i).getText());

		}
		
		
	}

}
