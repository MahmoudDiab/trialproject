package Techniques;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

public class SSIandChrome {

	public static void main(String[] args) {

		ChromeOptions option = new ChromeOptions();
		option.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		option.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		option.addArguments("--incognito");
		option.addArguments("--mute-audio");
		
		
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(option);
		driver.get("https://www.google.com/");

	}

}
