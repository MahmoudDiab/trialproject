package Techniques;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Frame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://demo.guru99.com/test/guru99home/");
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		int count = driver.findElements(By.tagName("iframe")).size();
		System.out.println(count);
		
		//for(int i=0; i<count; i++)
		//{
		//	driver.switchTo().frame(i);
		//	int yes = driver.findElements(By.xpath("//img[@src='Jmeter720.png']")).size();
		//	System.out.println(yes);
		//	driver.switchTo().defaultContent();
		//}
		
		driver.switchTo().frame(1);
		driver.findElement(By.xpath("//img[@src='Jmeter720.png']")).click();
		
		
	}

}
