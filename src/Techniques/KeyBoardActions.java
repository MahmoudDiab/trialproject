package Techniques;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class KeyBoardActions {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.skyscanner.net/?AID=12951743&PID=8026753&SID=102867d9d23aacbb095762727ff6b0&associateid=AFF_TRA_00014_00002&utm_source=commission+junction&utm_medium=affiliate&utm_campaign=gl-travel-4705670-8026753&utm_content=gl-travel-12951743&_tck=b/fqwn9l6MTFqFPLF1/8ilVE0fESuLlDT8coKepLQog");
		
		WebElement fromtextbox = driver.findElement(By.id("fsc-origin-search"));
		fromtextbox.clear();
		fromtextbox.sendKeys("Amsterdam");
		fromtextbox.sendKeys(Keys.TAB);
		
		
		Actions action1 = new Actions(driver);
		action1.sendKeys("Amman").build().perform();
		
		for(int i=0; i<4; i++)
		{
			action1.sendKeys(Keys.TAB).build().perform();
		}
		
		action1.sendKeys(Keys.ENTER).build().perform();
	
	}

}
