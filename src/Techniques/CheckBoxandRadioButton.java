package Techniques;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBoxandRadioButton {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.expedia.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//span[@class='tab-label']")).click();
		
		System.out.println(driver.findElement(By.id("flight-returning-hp-flight")).isDisplayed());
		
		driver.findElement(By.id("flight-type-one-way-label-hp-flight")).click();
		System.out.println(driver.findElement(By.id("flight-returning-hp-flight")).isDisplayed());
		
		
		
	}

}
