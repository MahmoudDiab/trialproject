package Techniques;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseAction {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.ebay.com/");
				
		WebElement textbox = driver.findElement(By.id("gh-ac"));
		Actions action = new Actions(driver);
		action.moveToElement(textbox).click().keyDown(Keys.SHIFT).sendKeys("mahmoud").build().perform();
		
	}

}
