package SeleniumBasicMethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasicMethods {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\redlaptop\\Downloads\\Chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://facebook.com");
		
		String actualurl = driver.getCurrentUrl();
		
		if (actualurl.contentEquals("https://www.google.com"))
		{
			System.out.println("Pass");
		}
		else
			System.out.println("Failed");
		
		
		System.out.println(driver.getTitle());
		System.out.println(driver.getPageSource());

	}

}
