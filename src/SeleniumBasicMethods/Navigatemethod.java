package SeleniumBasicMethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Navigatemethod {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\redlaptop\\Downloads\\Chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://google.com");
		
		driver.navigate().to("http://facebook.com");
		 driver.navigate().back();
		 driver.navigate().forward();
		 driver.navigate().refresh();

	}

}
