package SeleniumBasicMethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ControlWindowSize {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\redlaptop\\Downloads\\Chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://google.com");
		
		//driver.manage().window().maximize();
		driver.manage().window().fullscreen();
		
		System.out.println(driver.getWindowHandle());
	}

}
