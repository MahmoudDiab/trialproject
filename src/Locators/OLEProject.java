package Locators;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class OLEProject {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://play.google.com/store?hl=en");
		
		//driver.findElement(By.xpath("(//span[@jsname='r4nke'])[2]")).click();
		
		driver.manage().window().maximize();
		
		List <WebElement> leftmenu = driver.findElements(By.xpath("//span[@jsname='r4nke']"));
		leftmenu.get(1).click();
		
		driver.navigate().refresh();
		
		
		String actualurl = driver.getCurrentUrl();
		if (actualurl.contentEquals("https://play.google.com/store/apps?hl=en"))
			System.out.println("Pass");
		else
			System.out.println("Fail");
		
		driver.findElement(By.xpath("//button[@id='action-dropdown-parent-Categories']")).click();
		
		WebElement Eduction = driver.findElement(By.xpath("(//a[@class='r2Osbf'])[11]"));
		Eduction.click();
		
		driver.navigate().refresh();
		WebElement textbox = driver.findElement(By.id("gbqfq"));
		Actions mouse = new Actions(driver);
		//mouse.moveToElement(textbox).click().sendKeys("TED").build().perform();
		mouse.moveToElement(textbox).click().keyDown(Keys.SHIFT).sendKeys("ted").build().perform();
		
		driver.findElement(By.id("gbqfb")).click();
		
		driver.navigate().refresh();
		
		WebElement tedicon = driver.findElement(By.xpath("//div[@class='WsMG1c nnK0zc']"));
		Actions mouse1 = new Actions(driver);
		mouse1.moveToElement(tedicon).doubleClick().build().perform();
		
		
	}

}
